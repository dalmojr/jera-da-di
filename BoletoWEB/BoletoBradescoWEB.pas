{******************************************************************************}
{ Projeto: BoletoBradescoWEB                                                   }
{                                                                              }
{ Fun��o: Gerar boletos via WEB para o banco Bradesco                          }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2018 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}

{*******************************************************************************
|* Historico
|*
*******************************************************************************}

unit BoletoBradescoWEB;

interface

uses
  SysUtils;

type
  TBoletoBradescoWEBRetorno = class;

  TBoletoBradescoWEBRegistro = class
  private
    FAgencia_pagador: String;
    FAplicar_multa: Boolean;
    FConta_pagador: String;
    FControle_participante: String;
    FData_limite_desconto: TDate;
    FDebito_automatico: Boolean;
    FEndereco_debito_automatico: String;
    FEspecie_titulo: String;
    FPrimeira_instrucao: String;
    FQtde_dias_juros: Integer;
    FQtde_dias_multa: Integer;
    FRateio_credito: Boolean;
    FRazao_conta_pagador: String;
    FSegunda_instrucao: String;
    FSequencia_registro: String;
    FTipo_inscricao_pagador: String;
    FTipo_ocorrencia: String;
    FValor_abatimento: Integer;
    FValor_desconto: Integer;
    FValor_desconto_bonificacao: Integer;
    FValor_iof: Integer;
    FValor_juros_mora: Integer;
    FValor_multa: Integer;
    FValor_percentual_multa: Integer;
  public
    constructor Create;
    property agencia_pagador: String read FAgencia_pagador write FAgencia_pagador;
    property aplicar_multa: Boolean read FAplicar_multa write FAplicar_multa;
    property conta_pagador: String read FConta_pagador write FConta_pagador;
    property controle_participante: String read FControle_participante write FControle_participante;
    property data_limite_desconto: TDate read FData_limite_desconto write FData_limite_desconto;
    property debito_automatico: Boolean read FDebito_automatico write FDebito_automatico;
    property endereco_debito_automatico: String read FEndereco_debito_automatico write FEndereco_debito_automatico;
    property especie_titulo: String read FEspecie_titulo write FEspecie_titulo;
    property primeira_instrucao: String read FPrimeira_instrucao write FPrimeira_instrucao;
    property qtde_dias_juros: Integer read FQtde_dias_juros write FQtde_dias_juros;
    property qtde_dias_multa: Integer read FQtde_dias_multa write FQtde_dias_multa;
    property rateio_credito: Boolean read FRateio_credito write FRateio_credito;
    property razao_conta_pagador: String read FRazao_conta_pagador write FRazao_conta_pagador;
    property segunda_instrucao: String read FSegunda_instrucao write FSegunda_instrucao;
    property sequencia_registro: String read FSequencia_registro write FSequencia_registro;
    property tipo_inscricao_pagador: String read FTipo_inscricao_pagador write FTipo_inscricao_pagador;
    property tipo_ocorrencia: String read FTipo_ocorrencia write FTipo_ocorrencia;
    property valor_abatimento: Integer read FValor_abatimento write FValor_abatimento;
    property valor_desconto: Integer read FValor_desconto write FValor_desconto;
    property valor_desconto_bonificacao: Integer read FValor_desconto_bonificacao write FValor_desconto_bonificacao;
    property valor_iof: Integer read FValor_iof write FValor_iof;
    property valor_juros_mora: Integer read FValor_juros_mora write FValor_juros_mora;
    property valor_multa: Integer read FValor_multa write FValor_multa;
    property valor_percentual_multa: Integer read FValor_percentual_multa write FValor_percentual_multa;
  end;

  TBoletoBradescoWEBInstrucoes = class
  private
    FInstrucao_linha_1: String;
    FInstrucao_linha_2: String;
    FInstrucao_linha_3: String;
    FInstrucao_linha_4: String;
    FInstrucao_linha_5: String;
    FInstrucao_linha_6: String;
    FInstrucao_linha_7: String;
    FInstrucao_linha_8: String;
    FInstrucao_linha_9: String;
    FInstrucao_linha_10: String;
    FInstrucao_linha_11: String;
    FInstrucao_linha_12: String;
  public
    property instrucao_linha_1: String read FInstrucao_linha_1 write FInstrucao_linha_1;
    property instrucao_linha_2: String read FInstrucao_linha_2 write FInstrucao_linha_2;
    property instrucao_linha_3: String read FInstrucao_linha_3 write FInstrucao_linha_3;
    property instrucao_linha_4: String read FInstrucao_linha_4 write FInstrucao_linha_4;
    property instrucao_linha_5: String read FInstrucao_linha_5 write FInstrucao_linha_5;
    property instrucao_linha_6: String read FInstrucao_linha_6 write FInstrucao_linha_6;
    property instrucao_linha_7: String read FInstrucao_linha_7 write FInstrucao_linha_7;
    property instrucao_linha_8: String read FInstrucao_linha_8 write FInstrucao_linha_8;
    property instrucao_linha_9: String read FInstrucao_linha_9 write FInstrucao_linha_9;
    property instrucao_linha_10: String read FInstrucao_linha_10 write FInstrucao_linha_10;
    property instrucao_linha_11: String read FInstrucao_linha_11 write FInstrucao_linha_11;
    property instrucao_linha_12: String read FInstrucao_linha_12 write FInstrucao_linha_12;
  end;

  TBoletoBradescoWEBBoleto = class
  private
    FBeneficiario: String;
    FCarteira: String;
    FData_emissao: TDate;
    FData_vencimento: TDate;
    FInstrucoes: TBoletoBradescoWEBInstrucoes;
    FMensagem_cabecalho: String;
    FNosso_numero: String;
    FRegistro: TBoletoBradescoWEBRegistro;
    FTipo_renderizacao: String;
    FUrl_logotipo: String;
    FValor_titulo: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    property beneficiario: String read FBeneficiario write FBeneficiario;
    property carteira: String read FCarteira write FCarteira;
    property data_emissao: TDate read FData_emissao write FData_emissao;
    property data_vencimento: TDate read FData_vencimento write FData_vencimento;
    property instrucoes: TBoletoBradescoWEBInstrucoes read FInstrucoes write FInstrucoes;
    property mensagem_cabecalho: String read FMensagem_cabecalho write FMensagem_cabecalho;
    property nosso_numero: String read FNosso_numero write FNosso_numero;
    property registro: TBoletoBradescoWEBRegistro read FRegistro write FRegistro;
    property tipo_renderizacao: String read FTipo_renderizacao write FTipo_renderizacao;
    property url_logotipo: String read FUrl_logotipo write FUrl_logotipo;
    property valor_titulo: Integer read FValor_titulo write FValor_titulo;
  end;

  TBoletoBradescoWEBEndereco = class
  private
    FBairro: String;
    FCep: String;
    FCidade: String;
    FComplemento: String;
    FLogradouro: String;
    FNumero: String;
    FUf: String;
  public
    property bairro: String read FBairro write FBairro;
    property cep: String read FCep write FCep;
    property cidade: String read FCidade write FCidade;
    property complemento: String read FComplemento write FComplemento;
    property logradouro: String read FLogradouro write FLogradouro;
    property numero: String read FNumero write FNumero;
    property uf: String read FUf write FUf;
  end;

  TBoletoBradescoWEBComprador = class
  private
    FDocumento: String;
    FEndereco: TBoletoBradescoWEBEndereco;
    FIp: String;
    FNome: String;
    FUser_agent: String;
  public
    constructor Create;
    destructor Destroy; override;
    property documento: String read FDocumento write FDocumento;
    property endereco: TBoletoBradescoWEBEndereco read FEndereco write FEndereco;
    property ip: String read FIp write FIp;
    property nome: String read FNome write FNome;
    property user_agent: String read FUser_agent write FUser_agent;
  end;

  TBoletoBradescoWEBPedido = class
  private
    FDescricao: String;
    FNumero: String;
    FValor: Integer;
  public
    constructor Create;
    property descricao: String read FDescricao write FDescricao;
    property numero: String read FNumero write FNumero;
    property valor: Integer read FValor write FValor;
  end;

  TBoletoBradescoWEB = class
  private
    FBoleto: TBoletoBradescoWEBBoleto;
    FComprador: TBoletoBradescoWEBComprador;
    FMeio_pagamento: String;
    FMerchant_id: String;
    FPedido: TBoletoBradescoWEBPedido;
    FToken_request_confirmacao_pagamento: String;
  public
    constructor Create;
    destructor Destroy; override;
    function SaveJSON{$IFDEF DEBUG}(AFileSave: string = ''){$ENDIF}: string;
    function Enviar(const AChaveSeguranca: string;
                    AHomologacao: Boolean): TBoletoBradescoWEBRetorno;
    property boleto: TBoletoBradescoWEBBoleto read FBoleto write FBoleto;
    property comprador: TBoletoBradescoWEBComprador read FComprador write FComprador;
    property meio_pagamento: String read FMeio_pagamento write FMeio_pagamento;
    property merchant_id: String read FMerchant_id write FMerchant_id;
    property pedido: TBoletoBradescoWEBPedido read FPedido write FPedido;
    property token_request_confirmacao_pagamento: String read FToken_request_confirmacao_pagamento write FToken_request_confirmacao_pagamento;
  end;

  TBoletoBradescoWEBRetornoStatus = class
  private
    FCodigo: Extended;
    FMensagem: String;
  public
    property codigo: Extended read FCodigo write FCodigo;
    property mensagem: String read FMensagem write FMensagem;
  end;

  TBoletoBradescoWEBRetornoBoleto = class
  private
    FData_geracao: TDateTime;
    FLinha_digitavel: String;
    FLinha_digitavel_formatada: String;
    FToken: String;
    FUrl_acesso: String;
    FValor_titulo: Extended;
  public
    property data_geracao: TDateTime read FData_geracao write FData_geracao;
    property linha_digitavel: String read FLinha_digitavel write FLinha_digitavel;
    property linha_digitavel_formatada: String read FLinha_digitavel_formatada write FLinha_digitavel_formatada;
    property token: String read FToken write FToken;
    property url_acesso: String read FUrl_acesso write FUrl_acesso;
    property valor_titulo: Extended read FValor_titulo write FValor_titulo;
  end;

  TBoletoBradescoWEBRetornoPedido = class
  private
    FDescricao: String;
    FNumero: String;
    FValor: Extended;
  public
    property descricao: String read FDescricao write FDescricao;
    property numero: String read FNumero write FNumero;
    property valor: Extended read FValor write FValor;
  end;

  TBoletoBradescoWEBRetorno = class
  private
    FBoleto: TBoletoBradescoWEBRetornoBoleto;
    FMeio_pagamento: String;
    FMerchant_id: String;
    FPedido: TBoletoBradescoWEBRetornoPedido;
    FStatus: TBoletoBradescoWEBRetornoStatus;
  public
    constructor Create;
    destructor Destroy; override;
    property boleto: TBoletoBradescoWEBRetornoBoleto read FBoleto write FBoleto;
    property meio_pagamento: String read FMeio_pagamento write FMeio_pagamento;
    property merchant_id: String read FMerchant_id write FMerchant_id;
    property pedido: TBoletoBradescoWEBRetornoPedido read FPedido write FPedido;
    property status: TBoletoBradescoWEBRetornoStatus read FStatus write FStatus;
    class function FromJSON(const Value: string): TBoletoBradescoWEBRetorno;
  end;

///////////////////////////////////////////////////////////////////////////////////////
///  Classes para consulta de boletos
///////////////////////////////////////////////////////////////////////////////////////

  TBoletoBradescoWebConsultaPaginacao = class
  private
    FCurrentOffset: Extended;
    FLimit: Extended;
    FNextOffset: Extended;
  public
    property currentOffset: Extended read FCurrentOffset write FCurrentOffset;
    property limit: Extended read FLimit write FLimit;
    property nextOffset: Extended read FNextOffset write FNextOffset;
  end;

  TBoletoBradescoWebConsultaPedidos = class
  private
    FData: String;
    FDataCredito: String;
    FDataPagamento: String;
    FErro: String;
    FLinhaDigitavel: String;
    FNumero: String;
    FStatus: String;
    FValor: string;
    FValorPago: string;
  public
    property data: String read FData write FData;
    property dataCredito: String read FDataCredito write FDataCredito;
    property dataPagamento: String read FDataPagamento write FDataPagamento;
    property erro: String read FErro write FErro;
    property linhaDigitavel: String read FLinhaDigitavel write FLinhaDigitavel;
    property numero: String read FNumero write FNumero;
    property status: String read FStatus write FStatus;
    property valor: string read FValor write FValor;
    property valorPago: string read FValorPago write FValorPago;
  end;

  TBoletoBradescoWebConsultaToken = class
  private
    FDataCriacao: String;
    FToken: String;
  public
    property dataCriacao: String read FDataCriacao write FDataCriacao;
    property token: String read FToken write FToken;
  end;

  TBoletoBradescoWebConsultaStatus = class
  private
    FCodigo: Extended;
    FMensagem: String;
  public
    property codigo: Extended read FCodigo write FCodigo;
    property mensagem: String read FMensagem write FMensagem;
  end;

  TBoletoBradescoWebConsulta = class
  private
    FPaging: TBoletoBradescoWebConsultaPaginacao;
    FPedidos: TArray<TBoletoBradescoWebConsultaPedidos>;
    FStatus: TBoletoBradescoWebConsultaStatus;
    FToken: TBoletoBradescoWebConsultaToken;
    FJson: string;
  public
    constructor Create;
    destructor Destroy; override;
    property paging: TBoletoBradescoWebConsultaPaginacao read FPaging write FPaging;
    property status: TBoletoBradescoWebConsultaStatus read FStatus write FStatus;
    property token: TBoletoBradescoWebConsultaToken read FToken write FToken;
    property pedidos: TArray<TBoletoBradescoWebConsultaPedidos> read FPedidos write FPedidos;
    property Json: string read FJson;
    class procedure GetToken(const AChaveSeguranca: string;
                             const AEmail: string;
                             const AMerchanID: string;
                             AHomologacao: Boolean;
                             var ARetorno:  TBoletoBradescoWebConsulta);
    class procedure GetBoletos(const AChaveSeguranca: string;
                               const AEmail: string;
                               const AMerchanID: string;
                               const AToken: string;
                               const ADataInicial: TDate;
                               const ADataFinal: TDate;
                               AHomologacao: Boolean;
                               var ARetorno:  TBoletoBradescoWebConsulta;
                               AOffSet: Word = 1;
                               ALimit: Word = 1500);
    class function FromJSON(const Value: string): TBoletoBradescoWebConsulta;
  end;

implementation

uses
  XSuperObject, // https://github.com/onryldz/x-superobject
  idHttp,
  Synacode, System.Classes, System.IOUtils, ACBrUtil;

const
  URLPadrao = 'https://meiosdepagamentobradesco.com.br/apiboleto/transacao';
  URLHomolog = 'https://homolog.meiosdepagamentobradesco.com.br/apiboleto/transacao';
  URLPadraoGetToken = 'https://meiosdepagamentobradesco.com.br/SPSConsulta/Authentication/%s';
  URLHomologGetToken = 'https://homolog.meiosdepagamentobradesco.com.br/SPSConsulta/Authentication/%s';
  URLPadraoGetBoletos = 'https://meiosdepagamentobradesco.com.br/SPSConsulta/GetOrderListPayment/%s/boleto?token=%s&dataInicial=%s&dataFinal=%s';
  URLHomologGetBoletos = 'https://homolog.meiosdepagamentobradesco.com.br/SPSConsulta/GetOrderListPayment/%s/boleto?token=%s&dataInicial=%s&dataFinal=%s';

{TBoletoBradescoWEBBoleto}

constructor TBoletoBradescoWEBBoleto.Create;
begin
  inherited;
  FInstrucoes := TBoletoBradescoWEBInstrucoes.Create();
  FRegistro := TBoletoBradescoWEBRegistro.Create();
  FValor_titulo := 0;
end;

destructor TBoletoBradescoWEBBoleto.Destroy;
begin
  FInstrucoes.Free;
  FRegistro.Free;
  inherited;
end;

{TBoletoBradescoWEBComprador}

constructor TBoletoBradescoWEBComprador.Create;
begin
  inherited;
  FEndereco := TBoletoBradescoWEBEndereco.Create();
end;

destructor TBoletoBradescoWEBComprador.Destroy;
begin
  FEndereco.Free;
  inherited;
end;

{TBoletoBradescoWEB}

constructor TBoletoBradescoWEB.Create;
begin
  inherited;
  FPedido := TBoletoBradescoWEBPedido.Create();
  FComprador := TBoletoBradescoWEBComprador.Create();
  FBoleto := TBoletoBradescoWEBBoleto.Create();
end;

destructor TBoletoBradescoWEB.Destroy;
begin
  FPedido.Free;
  FComprador.Free;
  FBoleto.Free;
  inherited;
end;

function TBoletoBradescoWEB.Enviar(const AChaveSeguranca: string;
                                   AHomologacao: Boolean): TBoletoBradescoWEBRetorno;
var
  OHttp: TidHttp;
  SResult: string;
  SJson: TStringStream;
  ORetorno: TBoletoBradescoWEBRetorno;
  SAuth, SAuth64: string;
  ICodeHttp: Integer;
  SCodeHttp: string;
{$IFDEF DEBUG}
  SFileJSON: string;
  SDir: string;
  OStr: TStringList;
{$ENDIF}
begin
  Result := nil;
{$IFDEF DEBUG}
  SDir := TPath.Combine(TDirectory.GetCurrentDirectory, 'BoletoWEB');
  if not TDirectory.Exists(SDir) then
    TDirectory.CreateDirectory(SDir);
  SFileJSON := TPath.Combine(SDir, TGuid.NewGuid.ToString);
{$ENDIF}
  try
    OHttp := TidHttp.Create(nil);
    SJson := TStringStream.Create(SaveJSON{$IFDEF DEBUG}(SFileJSON + '.json'){$ENDIF}, TEncoding.UTF8);
    try
      SAuth := merchant_id + ':' + AChaveSeguranca;
      SAuth64 := String(EncodeBase64(AnsiString(SAuth)));
      OHTTP.Request.Accept := 'application/json';
      OHTTP.Request.AcceptCharSet := 'utf-8';
      OHTTP.Request.ContentType := 'application/json;UTF-8';
      OHTTP.Request.CharSet := 'utf-8';
      OHTTP.Request.CustomHeaders.FoldLines := False;
      OHTTP.Request.CustomHeaders.AddValue('Authorization', 'Basic ' + SAuth64);
      try
        if AHomologacao then
          SResult := OHTTP.Post(URLHomolog, SJson)
        else
          SResult := OHTTP.Post(URLPadrao, SJson);
        ICodeHttp := OHTTP.ResponseCode;
      except
        ICodeHttp := OHTTP.ResponseCode;
      end;
      if (ICodeHttp <> 200) and
         (ICodeHttp <> 201) then
      begin
        case ICodeHttp of
          401:
            raise Exception.Create('401 - Credencias de acesso n�o est�o presentes no cabe�alho da requisi��o BASE_64(MerchantID:ChaveDeSeguranca)');
          415:
            raise Exception.Create('415 - Tipo de conte�do da mensagem n�o suportado Valores v�lidos: application/json ou application/xml');
          400:
            raise Exception.Create('400 - Conte�do da mensagem vazio ou mal formatado');
          503:
            raise Exception.Create('503 - Erro ao processar requisi��o. Necess�rio acionar suporte t�cnico');
        else
            raise Exception.Create(ICodeHttp.ToString + ' - ' + OHTTP.ResponseText);
        end;
      end;
{$IFDEF DEBUG}
      OStr := TStringList.Create;
      try
        OStr.Text := SResult;
        OStr.SaveToFile(SFileJSON + 'Result.json');
      finally
        FreeAndNil(OStr);
      end;
{$ENDIF}
      Result := TBoletoBradescoWEBRetorno.FromJSON(SResult);
      if Result.status.codigo <> 0 then
      begin
        if Result.status.codigo = -399 then
          raise Exception.Create('Dados m�nimos da requisi��o n�o informado (Verifique: merchantid, orderid, codigo do meio de pagamento e chave da loja)')
        else if Result.status.codigo = -395 then
          raise Exception.Create('Formato do campo orderid invalido')
        else if Result.status.codigo = -400 then
          raise Exception.Create('C�digo da loja n�o encontrado')
        else if Result.status.codigo = -398 then
          raise Exception.Create('Loja informa um Merchant no corpo da mensagem e outro no header (Authorization)')
        else if Result.status.codigo = -518 then
          raise Exception.Create('A chave informada no header n�o confere com a chave do meio de pagamento cadastrada na base')
        else if Result.status.codigo = -394 then
          raise Exception.Create('O nosso n�mero informado pertence a outro titulo')
        else if Result.status.codigo = -902 then
          raise Exception.Create('Erro ao realizar a comunica��o com a loja (url de confirma��o do pedido)')
        else if Result.status.codigo = -401 then
          raise Exception.Create('C�digo de compra j� autorizado')
        else if Result.status.codigo = -999 then
          raise Exception.Create('Compra j� foi autorizada para este n�mero de pedido')
        else if Result.status.codigo = -397 then
          raise Exception.Create('A ordem de compra existe na orders (esta paga), por�m n�o existe na tb_boletos')
        else if Result.status.codigo = -396 then
          raise Exception.Create('Erro ao recuperar logotipo da loja')
        else if Result.status.codigo = -527 then
          raise Exception.Create('Documento na blacklist CPF/CNPJ')
        else if Result.status.codigo = -513 then
          raise Exception.Create('Erro - O valor da Tag VALOR n�o pode ser igual a zero ou Nulo')
        else if Result.status.codigo = -514 then
          raise Exception.Create('Erro - A Tag CEDENTE do boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -516 then
          raise Exception.Create('Erro - A Tag NUMEROAGENCIA do boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -5162 then
          raise Exception.Create('Erro - O n�mero da ag�ncia deve ter no m�ximo 4 d�gitos.')
        else if Result.status.codigo = -517 then
          raise Exception.Create('Erro - A Tag NUMEROCONTA do boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -5163 then
          raise Exception.Create('Erro - O n�mero da conta deve ter no m�ximo 7 d�gitos.')
        else if Result.status.codigo = -519 then
          raise Exception.Create('Erro - A Tag DATAEMISSAO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -520 then
          raise Exception.Create('Erro - A Tag DATAPROCESSAMENTO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -521 then
          raise Exception.Create('Erro - A Tag DATAVENCIMENTO do Boleto n�o foi encontrada ou n�o foi encontrada')
        else if Result.status.codigo = -522 then
          raise Exception.Create('Erro - A Tag NOMESACADO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -523 then
          raise Exception.Create('Erro - A Tag ENDERECOSACADO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -524 then
          raise Exception.Create('Erro - A Tag CIDADESACADO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -525 then
          raise Exception.Create('Erro - A Tag UFSACADO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -526 then
          raise Exception.Create('Erro - A Tag CEPSACADO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -527 then
          raise Exception.Create('Erro - A Tag CPFSACADO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -528 then
          raise Exception.Create('Erro - A Tag NUMEROPEDIDO do Boleto n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -529 then
          raise Exception.Create('Erro - A Tag VALORDOCUMENTOFORMATADO n�o foi encontrada ou est� malformada')
        else if Result.status.codigo = -530 then
          raise Exception.Create('Erro - A Tag SHOPPINGID n�o foi encontrada ou est� mal-formada')
        else if Result.status.codigo = -531 then
          raise Exception.Create('Erro - A Tag NUMERODOCUMENTO do Boleto n�o foi encontrada ou est� mal formatada')
        else if Result.status.codigo = -545 then
          raise Exception.Create('Valor m�ximo deve ser R$1,00')
        else if Result.status.codigo = -546 then
          raise Exception.Create('Carteira de cobran�a invalida (Diferente de 25/26)')
        else if Result.status.codigo = -548 then
          raise Exception.Create('Falha na comunica��o')
        else if Result.status.codigo = 93005117 then
          raise Exception.Create('Registro n�o encontrado nas bases CDDA/CIP')
        else if Result.status.codigo = 93005118 then
          raise Exception.Create('Informa��es de entrada inconsistentes CDDA/CIP')
        else if Result.status.codigo = 93005119 then
          raise Exception.Create('Registro efetuado com sucesso - CIP CONFIRMADA')
        else if Result.status.codigo = 93005120 then
          raise Exception.Create('Carteira de cobran�a n�o aceita');
      end;
    finally
      FreeAndNil(SJson);
      FreeAndNil(OHttp);
    end;
  except
    on E: Exception do
    begin
      raise Exception.Create('Erro ao enviar boleto!' + sLineBreak +
                             'A mensagem do erro est� abaixo:' + sLineBreak + sLineBreak +
                              E.Message);
    end;
  end;
end;

function TBoletoBradescoWEB.SaveJSON{$IFDEF DEBUG}(AFileSave: string = ''){$ENDIF}: string;
var
  SJ, SB, SBI, SBR, SP, SC, SCE: ISuperObject;
{$IFDEF DEBUG}
  OStr: TStringList;
{$ENDIF}
begin
  SJ := SO;

  {$REGION 'Dados b�sicos'}
  SJ.S['meio_pagamento'] := meio_pagamento;
  SJ.S['merchant_id'] := merchant_id;
  {$ENDREGION}

  {$REGION 'Pedido'}
  SP := SJ.O['pedido'];
  SP.S['numero'] := Pedido.numero;
  SP.I['valor'] := Pedido.valor;
  SP.S['descricao'] := Pedido.descricao;
  {$ENDREGION}

  {$REGION 'Comprador'}
  SC := SJ.O['comprador'];
  SC.S['nome'] := comprador.nome;
  SC.S['documento'] := comprador.documento;
  {$REGION 'Endere�o do Comprador'}
  SCE := SC.O['endereco'];
  SCE.S['cep'] := comprador.endereco.cep;
  SCE.S['logradouro'] := comprador.endereco.logradouro;
  SCE.S['numero'] := comprador.endereco.numero;
  SCE.S['complemento'] := comprador.endereco.complemento;
  SCE.S['bairro'] := comprador.endereco.bairro;
  SCE.S['cidade'] := comprador.endereco.cidade;
  SCE.S['uf'] := comprador.endereco.uf;
  {$ENDREGION}
  if Comprador.ip <> '' then
    SC.S['ip'] := comprador.ip
  else
    SC.Null['ip'] := jNull;
  if Comprador.user_agent <> '' then
    SC.S['user_agent'] := comprador.user_agent
  else
    SC.Null['user_agent'] := jNull;
  {$ENDREGION}

  {$REGION 'Boleto'}
  SB := SJ.O['boleto'];
  SB.S['beneficiario'] := boleto.beneficiario;
  SB.S['carteira'] := boleto.carteira;
  if boleto.nosso_numero <> '' then
    SB.S['nosso_numero'] := PadLeft(boleto.nosso_numero, 11, '0')
  else
    SB.Null['nosso_numero'] := jNull;
  SB.Date['data_emissao'] :=  boleto.data_emissao;
  SB.Date['data_vencimento'] := boleto.data_vencimento;
  SB.I['valor_titulo'] := boleto.valor_titulo;
  SB.S['url_logotipo'] := boleto.url_logotipo;
  SB.S['mensagem_cabecalho'] := boleto.mensagem_cabecalho;
  if Boleto.tipo_renderizacao <> '' then
    SB.S['tipo_renderizacao'] := Boleto.tipo_renderizacao;

  {$REGION 'Instru��es do Boleto'}
  SBI := SB.O['instrucoes'];
  if Boleto.instrucoes.instrucao_linha_1 <> '' then
    SBI.S['instrucao_linha_1'] := Boleto.instrucoes.instrucao_linha_1
  else
    SBI.Null['instrucao_linha_1'] := jNull;
  if Boleto.instrucoes.instrucao_linha_2 <> '' then
    SBI.S['instrucao_linha_2'] := Boleto.instrucoes.instrucao_linha_2
  else
    SBI.Null['instrucao_linha_2'] := jNull;
  if Boleto.instrucoes.instrucao_linha_3 <> '' then
    SBI.S['instrucao_linha_3'] := Boleto.instrucoes.instrucao_linha_3
  else
    SBI.Null['instrucao_linha_3'] := jNull;
  if Boleto.instrucoes.instrucao_linha_4 <> '' then
    SBI.S['instrucao_linha_4'] := Boleto.instrucoes.instrucao_linha_4
  else
    SBI.Null['instrucao_linha_4'] := jNull;
  if Boleto.instrucoes.instrucao_linha_5 <> '' then
    SBI.S['instrucao_linha_5'] := Boleto.instrucoes.instrucao_linha_5
  else
    SBI.Null['instrucao_linha_5'] := jNull;
  if Boleto.instrucoes.instrucao_linha_6 <> '' then
    SBI.S['instrucao_linha_6'] := Boleto.instrucoes.instrucao_linha_6
  else
    SBI.Null['instrucao_linha_6'] := jNull;
  if Boleto.instrucoes.instrucao_linha_7 <> '' then
    SBI.S['instrucao_linha_7'] := Boleto.instrucoes.instrucao_linha_7
  else
    SBI.Null['instrucao_linha_7'] := jNull;
  if Boleto.instrucoes.instrucao_linha_8 <> '' then
    SBI.S['instrucao_linha_8'] := Boleto.instrucoes.instrucao_linha_8
  else
    SBI.Null['instrucao_linha_8'] := jNull;
  if Boleto.instrucoes.instrucao_linha_9 <> '' then
    SBI.S['instrucao_linha_9'] := Boleto.instrucoes.instrucao_linha_9
  else
    SBI.Null['instrucao_linha_9'] := jNull;
  if Boleto.instrucoes.instrucao_linha_10 <> '' then
    SBI.S['instrucao_linha_10'] := Boleto.instrucoes.instrucao_linha_10
  else
    SBI.Null['instrucao_linha_10'] := jNull;
  if Boleto.instrucoes.instrucao_linha_11 <> '' then
    SBI.S['instrucao_linha_11'] := Boleto.instrucoes.instrucao_linha_11
  else
    SBI.Null['instrucao_linha_11'] := jNull;
  if Boleto.instrucoes.instrucao_linha_12 <> '' then
    SBI.S['instrucao_linha_12'] := Boleto.instrucoes.instrucao_linha_12
  else
    SBI.Null['instrucao_linha_12'] := jNull;
  {$ENDREGION}

  {$REGION 'Registro'}
  SBR := SB.O['registro'];
  SBR.S['agencia_pagador'] := Boleto.registro.agencia_pagador;
  SBR.S['razao_conta_pagador'] := Boleto.registro.razao_conta_pagador;
  SBR.S['conta_pagador'] := Boleto.registro.conta_pagador;
  SBR.B['aplicar_multa'] := Boleto.registro.aplicar_multa;
  if Boleto.registro.controle_participante <> '' then
    SBR.S['controle_participante'] := Boleto.registro.controle_participante
  else
    SBR.Null['controle_participante'] := jNull;
  if Boleto.registro.qtde_dias_multa > 0 then
    SBR.I['qtde_dias_multa'] := Boleto.registro.qtde_dias_multa
  else
    SBR.Null['qtde_dias_multa'] := jNull;
  if Boleto.registro.valor_percentual_multa > 0 then
    SBR.I['valor_percentual_multa'] := Boleto.registro.valor_percentual_multa
  else
    SBR.Null['valor_percentual_multa'] := jNull;
  if Boleto.registro.valor_multa > 0 then
    SBR.I['valor_multa'] := Boleto.registro.valor_multa
  else
    SBR.Null['valor_multa'] := jNull;
  if Boleto.registro.valor_desconto_bonificacao > 0 then
    SBR.I['valor_desconto_bonificacao'] := Boleto.registro.valor_desconto_bonificacao
  else
    SBR.Null['valor_desconto_bonificacao'] := jNull;
  SBR.B['debito_automatico'] := Boleto.registro.debito_automatico;
  SBR.B['rateio_credito'] := Boleto.registro.rateio_credito;
  SBR.S['endereco_debito_automatico'] := boleto.registro.endereco_debito_automatico;
  SBR.S['tipo_ocorrencia'] := boleto.registro.tipo_ocorrencia;
  SBR.S['especie_titulo'] := boleto.registro.especie_titulo;
  SBR.S['primeira_instrucao'] := boleto.registro.primeira_instrucao;
  SBR.S['segunda_instrucao'] := boleto.registro.segunda_instrucao;
  if boleto.registro.qtde_dias_juros > 0 then
    SBR.I['qtde_dias_juros'] := boleto.registro.qtde_dias_juros
  else
    SBR.Null['qtde_dias_juros'] := jNull;
  if boleto.registro.valor_juros_mora > 0 then
    SBR.I['valor_juros_mora'] := boleto.registro.valor_juros_mora
  else
    SBR.Null['valor_juros_mora'] := jNull;
  if boleto.registro.data_limite_desconto > 0 then
    SBR.S['data_limite_desconto'] := FormatDateTime('ddmmaa', boleto.registro.data_limite_desconto)
  else
    SBR.Null['data_limite_desconto'] :=jNull;
  if boleto.registro.valor_desconto > 0 then
    SBR.I['valor_desconto'] := boleto.registro.valor_desconto
  else
    SBR.Null['valor_desconto'] := jNull;
  SBR.I['valor_iof'] := 0;
  SBR.I['valor_abatimento'] := 0;
  if boleto.registro.tipo_inscricao_pagador = '' then
  begin
    if Length(comprador.documento) < 14 then
      SBR.S['tipo_inscricao_pagador'] := '01'
    else
      SBR.S['tipo_inscricao_pagador'] := '02';
  end;
  SBR.S['sequencia_registro'] := boleto.registro.sequencia_registro;
  {$ENDREGION}
  {$ENDREGION}

  {$REGION 'Token'}
  if token_request_confirmacao_pagamento <> '' then
    SJ.S['token_request_confirmacao_pagamento'] := token_request_confirmacao_pagamento
  else
    SJ.Null['token_request_confirmacao_pagamento'] := jNull;
  {$ENDREGION}

  Result := SJ.AsJSON;
{$IFDEF DEBUG}
  if AFileSave <> '' then
  begin
    OStr := TStringList.Create;
    try
      OStr.Text := Result;
      OStr.SaveToFile(AFileSave);
    finally
      FreeAndNil(OStr);
    end;
  end;
{$ENDIF}
//  Result := TJson.SuperObject<TBoletoBradescoWEB>(Self).AsJSON;
// JSON criado property por propery devido a algumas exig�ncias conforme manual do banco
end;

{TBoletoBradescoWEBRetorno}

constructor TBoletoBradescoWEBRetorno.Create;
begin
  inherited;
  FPedido := TBoletoBradescoWEBRetornoPedido.Create();
  FBoleto := TBoletoBradescoWEBRetornoBoleto.Create();
  FStatus := TBoletoBradescoWEBRetornoStatus.Create();
end;

destructor TBoletoBradescoWEBRetorno.Destroy;
begin
  FPedido.Free;
  FBoleto.Free;
  FStatus.Free;
  inherited;
end;

class function TBoletoBradescoWEBRetorno.FromJSON(const Value: string): TBoletoBradescoWEBRetorno;
begin
  result := TJson.Parse<TBoletoBradescoWEBRetorno>(Value);
end;

{ TBoletoBradescoWEBRegistro }

constructor TBoletoBradescoWEBRegistro.Create;
begin
  inherited;
  FAgencia_pagador := '00000';
  FRazao_conta_pagador := '00000';
  FConta_pagador := '00000000';
  Fendereco_debito_automatico := '00';
  FPrimeira_instrucao := '00';
  FSegunda_instrucao := '00';
  Ftipo_ocorrencia := '01';
  Fespecie_titulo := '99';
  FAplicar_multa := False;
  FDebito_automatico := False;
  FValor_percentual_multa := 0;
  FValor_multa := 0;
  FQtde_dias_juros := 0;
  FQtde_dias_multa := 0;
  FValor_abatimento := 0;
  FValor_desconto := 0;
  FValor_desconto_bonificacao := 0;
  FValor_iof := 0;
  FValor_juros_mora := 0;
  FSequencia_registro := '00';
end;

{ TBoletoBradescoWEBPedido }

constructor TBoletoBradescoWEBPedido.Create;
begin
  inherited;
  FValor := 0;
end;

{TBoletoBradescoWebConsulta}

constructor TBoletoBradescoWebConsulta.Create;
begin
  inherited;
  FStatus := TBoletoBradescoWebConsultaStatus.Create();
  FToken := TBoletoBradescoWebConsultaToken.Create();
  FPaging := TBoletoBradescoWebConsultaPaginacao.Create();
end;

destructor TBoletoBradescoWebConsulta.Destroy;
var
  LpedidosItem: TBoletoBradescoWebConsultaPedidos;
begin
  for LpedidosItem in FPedidos do
    LpedidosItem.Free;
  FStatus.Free;
  FToken.Free;
  FPaging.Free;
  inherited;
end;

class function TBoletoBradescoWebConsulta.FromJSON(const Value: string): TBoletoBradescoWebConsulta;
begin
  result := TJson.Parse<TBoletoBradescoWebConsulta>(Value);
end;

class procedure TBoletoBradescoWebConsulta.GetBoletos(const AChaveSeguranca: string;
                                                     const AEmail: string;
                                                     const AMerchanID: string;
                                                     const AToken: string;
                                                     const ADataInicial: TDate;
                                                     const ADataFinal: TDate;
                                                     AHomologacao: Boolean;
                                                     var ARetorno:  TBoletoBradescoWebConsulta;
                                                     AOffSet: Word;
                                                     ALimit: Word);
var
  OHttp: TidHttp;
  SResult: string;
  ORetorno: TBoletoBradescoWEBRetorno;
  SAuth, SAuth64: string;
  SCodeHttp: string;
  SUri: string;
{$IFDEF DEBUG}
  SFileJSON: string;
  SDir: string;
  OStr: TStringList;
{$ENDIF}
begin
{$IFDEF DEBUG}
  SDir := TPath.Combine(TDirectory.GetCurrentDirectory, 'BoletoWEB');
  if not TDirectory.Exists(SDir) then
    TDirectory.CreateDirectory(SDir);
  SFileJSON := TPath.Combine(SDir, TGuid.NewGuid.ToString);
{$ENDIF}
  try
    OHttp := TidHttp.Create(nil);
    try
      SAuth := AEmail + ':' + AChaveSeguranca;
      SAuth64 := String(EncodeBase64(AnsiString(SAuth)));
      OHTTP.Request.Accept := 'application/json';
      OHTTP.Request.AcceptCharSet := 'utf-8';
      OHTTP.Request.ContentType := 'application/json;UTF-8';
      OHTTP.Request.CharSet := 'utf-8';
      OHTTP.Request.CustomHeaders.FoldLines := False;
      OHTTP.Request.CustomHeaders.AddValue('Authorization', 'Basic ' + SAuth64);
      if AHomologacao then
        SUri := Format(URLHomologGetBoletos, [AMerchanID, AToken, FormatDateTime('yyyy/mm/dd', ADataInicial), FormatDateTime('yyyy/mm/dd', ADataFinal)])
      else
        SUri := Format(URLPadraoGetBoletos, [AMerchanID, AToken, FormatDateTime('yyyy/mm/dd', ADataInicial), FormatDateTime('yyyy/mm/dd', ADataFinal)]);
      SUri := SUri + '&status=1' +
                     '&offset=' + AOffSet.ToString +
                     '&limit=' + ALimit.Tostring;
      SResult := OHTTP.Get(SUri);
{$IFDEF DEBUG}
      OStr := TStringList.Create;
      try
        OStr.Text := SResult;
        OStr.SaveToFile(SFileJSON + 'GetBoletos.json');
      finally
        FreeAndNil(OStr);
      end;
{$ENDIF}
      if Assigned(ARetorno) then
        FreeAndNil(ARetorno);
      ARetorno := TBoletoBradescoWebConsulta.FromJSON(SResult);
      ARetorno.FJson := SResult;
      if ARetorno.status.codigo <> 0 then
      begin
        raise Exception.Create(ARetorno.status.codigo.ToString + ': ' + ARetorno.status.mensagem);
      end;
    finally
      FreeAndNil(OHttp);
    end;
  except
    on E: Exception do
    begin
      raise Exception.Create('Erro ao efetuar consulta de Boletos!' + sLineBreak +
                             'A mensagem do erro est� abaixo:' + sLineBreak + sLineBreak +
                              E.Message);
    end;
  end;
end;

class procedure TBoletoBradescoWebConsulta.GetToken(const AChaveSeguranca: string;
                                                    const AEmail: string;
                                                    const AMerchanID: string;
                                                    AHomologacao: Boolean;
                                                    var ARetorno:  TBoletoBradescoWebConsulta);
var
  OHttp: TidHttp;
  SResult: string;
  ORetorno: TBoletoBradescoWEBRetorno;
  SAuth, SAuth64: string;
  SCodeHttp: string;
{$IFDEF DEBUG}
  SFileJSON: string;
  SDir: string;
  OStr: TStringList;
{$ENDIF}
begin
{$IFDEF DEBUG}
  SDir := TPath.Combine(TDirectory.GetCurrentDirectory, 'BoletoWEB');
  if not TDirectory.Exists(SDir) then
    TDirectory.CreateDirectory(SDir);
  SFileJSON := TPath.Combine(SDir, TGuid.NewGuid.ToString);
{$ENDIF}
  try
    OHttp := TidHttp.Create(nil);
    try
      SAuth := AEmail + ':' + AChaveSeguranca;
      SAuth64 := String(EncodeBase64(AnsiString(SAuth)));
      OHTTP.Request.Accept := 'application/json';
      OHTTP.Request.AcceptCharSet := 'utf-8';
      OHTTP.Request.ContentType := 'application/json;UTF-8';
      OHTTP.Request.CharSet := 'utf-8';
      OHTTP.Request.CustomHeaders.FoldLines := False;
      OHTTP.Request.CustomHeaders.AddValue('Authorization', 'Basic ' + SAuth64);
      if AHomologacao then
        SResult := OHTTP.Get(Format(URLHomologGetToken, [AMerchanID]))
      else
        SResult := OHTTP.Get(Format(URLPadraoGetToken, [AMerchanID]));
{$IFDEF DEBUG}
      OStr := TStringList.Create;
      try
        OStr.Text := SResult;
        OStr.SaveToFile(SFileJSON + 'GetToken.json');
      finally
        FreeAndNil(OStr);
      end;
{$ENDIF}
      if Assigned(ARetorno) then
        FreeAndNil(ARetorno);
      ARetorno := TBoletoBradescoWebConsulta.FromJSON(SResult);
      ARetorno.FJson := SResult;
      if ARetorno.status.codigo <> 0 then
      begin
        raise Exception.Create(ARetorno.status.codigo.ToString + ': ' + ARetorno.status.mensagem);
      end;
    finally
      FreeAndNil(OHttp);
    end;
  except
    on E: Exception do
    begin
      raise Exception.Create('Erro ao efetuar consulta Token!' + sLineBreak +
                             'A mensagem do erro est� abaixo:' + sLineBreak + sLineBreak +
                              E.Message);
    end;
  end;
end;

end.
