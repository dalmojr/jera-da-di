{******************************************************************************}
{ Projeto: TJeraDACEF                                                     }
{                                                                              }
{ Fun��o: Gerar arquivo de remessa e efetuar a a leitura de mesmo, referente   }
{         D�bitos Autorizados/Autom�tico - Banco CEF                      }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}
unit Jera.DA.CEF;

interface

uses
  System.SysUtils, System.Rtti, System.Generics.Collections, Jera.DA,
  Jera.DADI.Util;

type
//  Registro �A� - Header
//  Obrigat�rio em todos os arquivos.
  TJeraDACEFBlocoA = class(TInterfacedObject, IJeraDABlocoA)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 1, '')]
    [Enumerado('1,2')]
    FCodigoRemessa: TDARemessaRetorno;

    [DadosDefinicoes(fdatString, 3, 20, '')]
    FCodigoConvenio: string;

    [DadosDefinicoes(fdatString, 23, 20, '')]
    FNomedaEmpresa: string;

    [DadosDefinicoes(fdatString, 43, 3, '')]
    FCodigoBanco: string;

    [DadosDefinicoes(fdatString, 46, 20, '')]
    FNomeBanco: string;

    [DadosDefinicoes(fdatData, 66, 8, 'YYYYMMDD')]
    FDataGeracao: TDate;

    [DadosDefinicoes(fdatInteger, 74, 6, '')]
    FNSA: Integer;

    [DadosDefinicoes(fdatString, 80, 2, '')]
    FVersaoLayout: string;

    [DadosDefinicoes(fdatString, 82, 17, '')]
    FIdentificacaoServico: string;

    [DadosDefinicoes(fdatInteger, 99, 16, '')]
    FContaCompromisso: Integer;

    [DadosDefinicoes(fdatString, 115, 1, '')]
    [Enumerado('P,T')]
    FIdentificacaoAmbienteCliente: TDAAmbiente;

    [DadosDefinicoes(fdatString, 116, 1, '')]
    [Enumerado('P,T')]
    FIdentificacaoAmbienteBanco: TDAAmbiente;

    [DadosDefinicoes(fdatString, 117, 27, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 144, 6, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    FBrancos3: string;

    function GetCodigoRegistro: string;
    function GetCodigoRemessa: TDARemessaRetorno;
    procedure SetCodigoRemessa(const Value: TDARemessaRetorno);
    function GetCodigoConvenio: string;
    procedure SetCodigoConvenio(const Value: string);
    function GetNomedaEmpresa: string;
    procedure SetNomedaEmpresa(const Value: string);
    function GetCodigoBanco: string;
    function GetNomeBanco: string;
    function GetDataGeracao: TDate;
    procedure SetDataGeracao(const Value: TDate);
    function GetNSA: Integer;
    procedure SetNSA(const Value: Integer);
    function GetVersaoLayout: string;
    function GetIdentificacaoServico: string;
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetBrancos3: string;
    function GetContaCompromisso: Integer;
    procedure SetContaCompromisso(const Value: Integer);
    function GetIdentificacaoAmbienteCliente: TDAAmbiente;
    procedure SetIdentificacaoAmbienteCliente(const Value: TDAAmbiente);
    function GetIdentificacaoAmbienteBanco: TDAAmbiente;
    procedure SetIdentificacaoAmbienteBanco(const Value: TDAAmbiente);
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property CodigoRemessa: TDARemessaRetorno read GetCodigoRemessa write SetCodigoRemessa;
    property CodigoConvenio: string read GetCodigoConvenio write SetCodigoConvenio;
    property NomedaEmpresa: string read GetNomedaEmpresa write SetNomedaEmpresa;
    property CodigoBanco: string read GetCodigoBanco;
    property NomeBanco: string read GetNomeBanco;
    property DataGeracao: TDate read GetDataGeracao write SetDataGeracao;
    property NSA: Integer read GetNSA write SetNSA;
    property VersaoLayout: string read GetVersaoLayout;
    property IdentificacaoServico: string read GetIdentificacaoServico;
    property ContaCompromisso: Integer read GetContaCompromisso write SetContaCompromisso;
    property IdentificacaoAmbienteCliente: TDAAmbiente read GetIdentificacaoAmbienteCliente write SetIdentificacaoAmbienteCliente;
    property IdentificacaoAmbienteBanco: TDAAmbiente read GetIdentificacaoAmbienteBanco write SetIdentificacaoAmbienteBanco;
    property Brancos: string read GetBrancos;
    property Brancos2: string read GetBrancos2;
    property Brancos3: string read GetBrancos3;
  end;

  TJeraDACEFBlocoB = class(TInterfacedObject, IJeraDABlocoB)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FData: TDate;

    [DadosDefinicoes(fdatString, 53, 97, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('1,2')]
    FCodigoMovimento: TDACodigoMovimentoBlocoB;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetData: TDate;
    procedure SetData(const Value: TDate);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    procedure CarregarLinha(const Value: string);
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Identificacao: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property Data: TDate read GetData write SetData;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no CEF
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDACEFBlocoC = class(TInterfacedObject, IJeraDABlocoC)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatString, 45, 40, '')]
    [Enumerado('Identifica��o do cliente n�o localizada / inexistente,' +
               'Restri��o de cadastramento pela empresa,' +
               'Cliente cadastrado em outro Banco com data poster,' +
               'Operadora invalida,' +
               'Cliente desativado no cadastro da empresa')]
    FOcorrencia: TDAOcorrenciaBlocoC;

    [DadosDefinicoes(fdatString, 85, 40, '')]
    FOcorrencia2: string;

    [DadosDefinicoes(fdatString, 125, 25, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('1,2')]
    FCodigoMovimento: TDACodigoMovimentoBlocoB;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetBrancos1: string;
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetOcorrencia: TDAOcorrenciaBlocoC;
    procedure SetOcorrencia(const Value: TDAOcorrenciaBlocoC);
    function GetOcorrencia2: string;
    procedure SetOcorrencia2(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property Ocorrencia: TDAOcorrenciaBlocoC read GetOcorrencia write SetOcorrencia;
    property Ocorrencia2: string read GetOcorrencia2 write SetOcorrencia2;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no CEF
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos1: string read GetBrancos1;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDACEFBlocoD = class(TInterfacedObject, IJeraDABlocoD)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresaAnterior: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatString, 45, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 60, 70, '')]
    [Enumerado('Exclus�o por altera��o cadastral do cliente,' +
               'Exclus�o - transferido para d�bito em outro banco,' +
               'Exclus�o por insufici�ncia de fundos,' +
               'Exclus�o por solicita��o do cliente')]
    FOcorrencia: TDAOcorrenciaBlocoD;

    [DadosDefinicoes(fdatString, 20, 130, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 1, 150, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimentoBlocoD;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: TDAOcorrenciaBlocoD;
    procedure SetOcorrencia(const Value: TDAOcorrenciaBlocoD);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: TDAOcorrenciaBlocoD read GetOcorrencia write SetOcorrencia;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no CEF
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDACEFBlocoE = CLASS(TInterfacedObject, IJeraDABlocoE)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FDataVencimento: TDate;

    [DadosDefinicoes(fdatMoney, 53, 15, '')]
    FValorDebito: Currency;

    [DadosDefinicoes(fdatString, 68, 2, '')]
    [Enumerado('01,03')]
    FCodigoMoeda: TDACodigoMoeda;

    [DadosDefinicoes(fdatString, 70, 59, '')]
    FUsoEmpresa: string;

    [DadosDefinicoes(fdatString, 129, 1, '')]
    [Enumerado('@,X')]
    FTratamentoAcordado: Boolean;

    [DadosDefinicoes(fdatString, 130, 1, '')]
    [Enumerado('1,2,@')]
    FTipoIdentificacao: TDADIIdentificacao;

    [DadosDefinicoes(fdatString, 131, 15, '')]
    FIdentificacao: string;

    [DadosDefinicoes(fdatString, 146, 4, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1,5')]
    FCodigoMovimento: TDACodigoMovimento;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Currency;
    procedure SetValorDebito(const Value: Currency);
    function GetCodigoMoeda: TDACodigoMoeda;
    procedure SetCodigoMoeda(const Value: TDACodigoMoeda);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTratamentoAcordado: Boolean;
    procedure SetTratamentoAcordado(const Value: Boolean);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: string;
    procedure SetIdentificacao(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
    function GetValorMora: Currency;
    procedure SetValorMora(const Value: Currency);
    function GetComplemento: string;
    procedure SetComplemento(const Value: string);
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Currency read GetValorDebito write SetValorDebito;
    property CodigoMoeda: TDACodigoMoeda read GetCodigoMoeda write SetCodigoMoeda;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property TratamentoAcordado: Boolean read GetTratamentoAcordado write SetTratamentoAcordado;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property Identificacao: string read GetIdentificacao write SetIdentificacao;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no CEF
    property Brancos2: string read GetBrancos2;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property ValorMora: Currency read GetValorMora write SetValorMora;
    property Complemento: string read GetComplemento write SetComplemento;
  end;

  TJeraDACEFBlocoF = class(TInterfacedObject, IJeraDABlocoF)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatData, 45, 8, 'YYYYMMDD')]
    FDataVencimento: TDate;

    [DadosDefinicoes(fdatMoney, 53, 15, '')]
    FValorDebito: Currency;

    [DadosDefinicoes(fdatString, 68, 2, '')]
    [Enumerado('00,01,02,04,05,10,12,13,14,15,18,19,20,30,31,47,48,49,50,96,97,98,99')]
    FCodigoRetorno: TDACodigoRetorno;

    [DadosDefinicoes(fdatString, 70, 60, '')]
    FUsoEmpresa: string;

    [DadosDefinicoes(fdatString, 130, 1, '')]
    [Enumerado('1,2')]
    FTipoIdentificacao: TDADIIdentificacao;

    [DadosDefinicoes(fdatInteger, 131, 15, '')]
    FIdentificacao: Int64;

    [DadosDefinicoes(fdatString, 146, 4, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimento;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Currency;
    procedure SetValorDebito(const Value: Currency);
    function GetValorMora: Currency;
    procedure SetValorMora(const Value: Currency);
    function GetCodigoRetorno: TDACodigoRetorno;
    procedure SetCodigoRetorno(const Value: TDACodigoRetorno);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: Int64;
    procedure SetIdentificacao(const Value: Int64);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);
  public
    constructor Create;
    procedure CarregarLinha(const Value: string);
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Currency read GetValorDebito write SetValorDebito;
    property CodigoRetorno: TDACodigoRetorno read GetCodigoRetorno write SetCodigoRetorno;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property Identificacao: Int64 read GetIdentificacao write SetIdentificacao;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no CEF
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property ValorMora: Currency read GetValorMora write SetValorMora;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDACEFBlocoH = class(TInterfacedObject, IJeraDABlocoH)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresaAnterior: string;

    [DadosDefinicoes(fdatString, 27, 4, '')]
    FAgenciaDebito: string;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FIdentificacaoClienteBanco: string;

    [DadosDefinicoes(fdatString, 45, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 58, 70, '')]
    FOcorrencia: string;

    [DadosDefinicoes(fdatString, 128, 22, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    [Enumerado('0,1')]
    FCodigoMovimento: TDACodigoMovimentoBlocoD;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: string;
    procedure SetOcorrencia(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
  public
    constructor Create;
    procedure CarregarLinha(const Value: string);
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: string read GetOcorrencia write SetOcorrencia;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;

    // Apenas para vers�o 4 do Layout, N�o aplica no CEF
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

  TJeraDACEFBlocoI = class(TInterfacedObject, IJeraDABlocoI)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatString, 2, 25, '')]
    FIdentificacaoClienteEmpresa: string;

    [DadosDefinicoes(fdatString, 27, 1, '')]
    [Enumerado('1,2')]
    FTipoIdentificacao: TDADIIdentificacao;

    [DadosDefinicoes(fdatString, 31, 14, '')]
    FCnpjCpf: string;

    [DadosDefinicoes(fdatString, 45, 40, '')]
    FNomeConsumidor: string;

    [DadosDefinicoes(fdatString, 70, 30, '')]
    FCidadeConsumidor: string;

    [DadosDefinicoes(fdatString, 128, 2, '')]
    FUFConsumidor: string;

    [DadosDefinicoes(fdatString, 150, 37, '')]
    FBrancos: string;

    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetCnpjCpf: string;
    procedure SetCnpjCpf(const Value: string);
    function GetNomeConsumidor: string;
    procedure SetNomeConsumidor(const Value: string);
    function GetCidadeConsumidor: string;
    procedure SetCidadeConsumidor(const Value: string);
    function GetUFConsumidor: string;
    procedure SetUFConsumidor(const Value: string);
    function GetBrancos: string;
  public
    constructor Create;
    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property CnpjCpf: string read GetCnpjCpf write SetCnpjCpf;
    property NomeConsumidor: string read GetNomeConsumidor write SetNomeConsumidor;
    property CidadeConsumidor: string read GetCidadeConsumidor write SetCidadeConsumidor;
    property UFConsumidor: string read GetUFConsumidor write SetUFConsumidor;
    property Brancos: string read GetBrancos;
  end;

  TJeraDACEFBlocoZ = class(TInterfacedObject, IJeraDABlocoZ)
  private
    [DadosDefinicoes(fdatString, 1, 1, '')]
    FCodigoRegistro: string;

    [DadosDefinicoes(fdatInteger, 2, 6, '')]
    FTotalRegistrosArquivo: Integer;

    [DadosDefinicoes(fdatMoney, 8, 17, '')]
    FValorTotalRegistros: Currency;

    [DadosDefinicoes(fdatString, 25, 119, '')]
    FBrancos: string;

    [DadosDefinicoes(fdatString, 144, 6, '')]
    FBrancos2: string;

    [DadosDefinicoes(fdatString, 150, 1, '')]
    FBrancos3: string;

    function GetCodigoRegistro: string;
    function GetTotalRegistrosArquivo: Integer;
    procedure SetTotalRegistrosArquivo(const Value: Integer);
    function GetValorTotalRegistros: Currency;
    procedure SetValorTotalRegistros(const Value: Currency);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetBrancos3: string;
  public
    constructor Create;
    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property TotalRegistrosArquivo: Integer read GetTotalRegistrosArquivo write SetTotalRegistrosArquivo;
    property ValorTotalRegistros: Currency read GetValorTotalRegistros write SetValorTotalRegistros;
    property Brancos: string read GetBrancos;
    property Brancos1: string read GetBrancos2;
    property Brancos2: string read GetBrancos3;
  end;

implementation

{ TJeraDACEFBlocoA }

procedure TJeraDACEFBlocoA.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDACEFBlocoA.Create;
begin
  inherited;
  FCodigoRegistro := 'A';
  FCodigoRemessa := rrRemessa;
  FCodigoBanco := '104';
  FVersaoLayout := '05';
  FIdentificacaoServico := 'D�BITO AUTOM�TICO';
end;

function TJeraDACEFBlocoA.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoA.GetBrancos2: string;
begin
  FBrancos2 := '000000';
  Result := FBrancos2;
end;

function TJeraDACEFBlocoA.GetBrancos3: string;
begin
  FBrancos3 := '';
  Result := FBrancos3;
end;

function TJeraDACEFBlocoA.GetCodigoBanco: string;
begin
  Result := FCodigoBanco;
end;

function TJeraDACEFBlocoA.GetCodigoConvenio: string;
begin
  Result := FCodigoConvenio;
end;

function TJeraDACEFBlocoA.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoA.GetCodigoRemessa: TDARemessaRetorno;
begin
  Result := FCodigoRemessa;
end;

function TJeraDACEFBlocoA.GetContaCompromisso: Integer;
begin
  Result := FContaCompromisso;
end;

function TJeraDACEFBlocoA.GetDataGeracao: TDate;
begin
  Result := FDataGeracao;
end;

function TJeraDACEFBlocoA.GetIdentificacaoAmbienteBanco: TDAAmbiente;
begin
  Result := FIdentificacaoAmbienteBanco;
end;

function TJeraDACEFBlocoA.GetIdentificacaoAmbienteCliente: TDAAmbiente;
begin
  Result := FIdentificacaoAmbienteCliente;
end;

function TJeraDACEFBlocoA.GetIdentificacaoServico: string;
begin
  Result := FIdentificacaoServico;
end;

function TJeraDACEFBlocoA.GetNomeBanco: string;
begin
  FNomeBanco := 'BANCO CEF';
  Result := FNomeBanco;
end;

function TJeraDACEFBlocoA.GetNomedaEmpresa: string;
begin
  Result := FNomedaEmpresa;
end;

function TJeraDACEFBlocoA.GetNSA: Integer;
begin
  Result := FNSA;
end;

function TJeraDACEFBlocoA.GetVersaoLayout: string;
begin
  Result := FVersaoLayout;
end;

function TJeraDACEFBlocoA.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoA.SetCodigoConvenio(const Value: string);
begin
  FCodigoConvenio := Value;
end;

procedure TJeraDACEFBlocoA.SetCodigoRemessa(const Value: TDARemessaRetorno);
begin
  FCodigoRemessa := Value;
end;

procedure TJeraDACEFBlocoA.SetContaCompromisso(const Value: Integer);
begin
  FContaCompromisso := Value;
end;

procedure TJeraDACEFBlocoA.SetDataGeracao(const Value: TDate);
begin
  FDataGeracao := Value;
end;

procedure TJeraDACEFBlocoA.SetIdentificacaoAmbienteBanco(const Value: TDAAmbiente);
begin
  FIdentificacaoAmbienteBanco := Value;
end;

procedure TJeraDACEFBlocoA.SetIdentificacaoAmbienteCliente(const Value: TDAAmbiente);
begin
  FIdentificacaoAmbienteCliente := Value;
end;

procedure TJeraDACEFBlocoA.SetNomedaEmpresa(const Value: string);
begin
  FNomedaEmpresa := Value;
end;

procedure TJeraDACEFBlocoA.SetNSA(const Value: Integer);
begin
  FNSA := Value;
end;

{ TJeraDACEFBlocoB }

procedure TJeraDACEFBlocoB.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDACEFBlocoB.Create;
begin
  inherited Create;
  FCodigoRegistro := 'B';
end;

function TJeraDACEFBlocoB.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDACEFBlocoB.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoB.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDACEFBlocoB.GetCodigoMovimento: TDACodigoMovimentoBlocoB;
begin
  Result := FCodigoMovimento;
end;

function TJeraDACEFBlocoB.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoB.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                          'Conta');
end;

function TJeraDACEFBlocoB.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                          'D�gito da Conta');
end;

function TJeraDACEFBlocoB.GetData: TDate;
begin
  Result := FData;
end;

function TJeraDACEFBlocoB.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDACEFBlocoB.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDACEFBlocoB.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoB.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDACEFBlocoB.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoB);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDACEFBlocoB.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDACEFBlocoB.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDACEFBlocoB.SetData(const Value: TDate);
begin
  FData := Value;
end;

procedure TJeraDACEFBlocoB.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDACEFBlocoB.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

{ TJeraDACEFBlocoC }

constructor TJeraDACEFBlocoC.Create;
begin
  inherited Create;
  FCodigoRegistro := 'C';
end;

function TJeraDACEFBlocoC.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDACEFBlocoC.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoC.GetBrancos1: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos1');
end;

function TJeraDACEFBlocoC.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDACEFBlocoC.GetCodigoMovimento: TDACodigoMovimentoBlocoB;
begin
  Result := FCodigoMovimento;
end;

function TJeraDACEFBlocoC.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoC.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDACEFBlocoC.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDACEFBlocoC.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDACEFBlocoC.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDACEFBlocoC.GetOcorrencia: TDAOcorrenciaBlocoC;
begin
  Result := FOcorrencia;
end;

function TJeraDACEFBlocoC.GetOcorrencia2: string;
begin
  Result := FOcorrencia2;
end;

function TJeraDACEFBlocoC.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoC.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDACEFBlocoC.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoB);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDACEFBlocoC.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDACEFBlocoC.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDACEFBlocoC.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDACEFBlocoC.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDACEFBlocoC.SetOcorrencia(const Value: TDAOcorrenciaBlocoC);
begin
  FOcorrencia := Value;
end;

procedure TJeraDACEFBlocoC.SetOcorrencia2(const Value: string);
begin
  FOcorrencia2 := Value;
end;

{ TJeraDACEFBlocoD }

constructor TJeraDACEFBlocoD.Create;
begin
  inherited Create;
  FCodigoRegistro := 'D';
end;

function TJeraDACEFBlocoD.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDACEFBlocoD.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoD.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDACEFBlocoD.GetCodigoMovimento: TDACodigoMovimentoBlocoD;
begin
  Result := FCodigoMovimento;
end;

function TJeraDACEFBlocoD.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoD.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDACEFBlocoD.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDACEFBlocoD.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDACEFBlocoD.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDACEFBlocoD.GetIdentificacaoClienteEmpresaAnterior: string;
begin
  Result := FIdentificacaoClienteEmpresaAnterior;
end;

function TJeraDACEFBlocoD.GetOcorrencia: TDAOcorrenciaBlocoD;
begin
  Result := FOcorrencia;
end;

function TJeraDACEFBlocoD.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoD.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDACEFBlocoD.SetCodigoMovimento(
  const Value: TDACodigoMovimentoBlocoD);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDACEFBlocoD.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDACEFBlocoD.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDACEFBlocoD.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDACEFBlocoD.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDACEFBlocoD.SetIdentificacaoClienteEmpresaAnterior(
  const Value: string);
begin
  FIdentificacaoClienteEmpresaAnterior := Value;
end;

procedure TJeraDACEFBlocoD.SetOcorrencia(const Value: TDAOcorrenciaBlocoD);
begin
  FOcorrencia := Value;
end;

{ TJeraDACEFBlocoE }

constructor TJeraDACEFBlocoE.Create;
begin
  inherited Create;
  FCodigoRegistro := 'E';
end;

function TJeraDACEFBlocoE.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDACEFBlocoE.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoE.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDACEFBlocoE.GetCodigoMoeda: TDACodigoMoeda;
begin
  Result := FCodigoMoeda;
end;

function TJeraDACEFBlocoE.GetCodigoMovimento: TDACodigoMovimento;
begin
  Result := FCodigoMovimento;
end;

function TJeraDACEFBlocoE.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoE.GetComplemento: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Complemento');
end;

function TJeraDACEFBlocoE.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDACEFBlocoE.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDACEFBlocoE.GetDataVencimento: TDate;
begin
  Result := FDataVencimento;
end;

function TJeraDACEFBlocoE.GetIdentificacao: string;
begin
  Result := FIdentificacao;
end;

function TJeraDACEFBlocoE.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDACEFBlocoE.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDACEFBlocoE.GetTipoIdentificacao: TDADIIdentificacao;
begin
  Result := FTipoIdentificacao;
end;

function TJeraDACEFBlocoE.GetTratamentoAcordado: Boolean;
begin
  Result := FTratamentoAcordado;
end;

function TJeraDACEFBlocoE.GetUsoEmpresa: string;
begin
  Result := FUsoEmpresa;
end;

function TJeraDACEFBlocoE.GetValorDebito: Currency;
begin
  Result := FValorDebito;
end;

function TJeraDACEFBlocoE.GetValorMora: Currency;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

function TJeraDACEFBlocoE.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoE.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDACEFBlocoE.SetCodigoMoeda(const Value: TDACodigoMoeda);
begin
  FCodigoMoeda := Value;
end;

procedure TJeraDACEFBlocoE.SetCodigoMovimento(
  const Value: TDACodigoMovimento);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDACEFBlocoE.SetComplemento(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Complemento');
end;

procedure TJeraDACEFBlocoE.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDACEFBlocoE.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDACEFBlocoE.SetDataVencimento(const Value: TDate);
begin
  FDataVencimento := Value;
end;

procedure TJeraDACEFBlocoE.SetIdentificacao(const Value: string);
begin
  FIdentificacao := Value;
end;

procedure TJeraDACEFBlocoE.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDACEFBlocoE.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDACEFBlocoE.SetTipoIdentificacao(
  const Value: TDADIIdentificacao);
begin
  FTipoIdentificacao := Value;
end;

procedure TJeraDACEFBlocoE.SetTratamentoAcordado(const Value: Boolean);
begin
  FTratamentoAcordado := Value;
end;

procedure TJeraDACEFBlocoE.SetUsoEmpresa(const Value: string);
begin
  FUsoEmpresa := Value;
end;

procedure TJeraDACEFBlocoE.SetValorDebito(const Value: Currency);
begin
  FValorDebito := Value;
end;

procedure TJeraDACEFBlocoE.SetValorMora(const Value: Currency);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

{ TJeraDACEFBlocoF }

procedure TJeraDACEFBlocoF.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDACEFBlocoF.Create;
begin
  inherited Create;
  FCodigoRegistro := 'F';
end;

function TJeraDACEFBlocoF.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDACEFBlocoF.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoF.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDACEFBlocoF.GetCodigoMovimento: TDACodigoMovimento;
begin
  Result := FCodigoMovimento;
end;

function TJeraDACEFBlocoF.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoF.GetCodigoRetorno: TDACodigoRetorno;
begin
  Result := FCodigoRetorno;
end;

function TJeraDACEFBlocoF.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDACEFBlocoF.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDACEFBlocoF.GetDataVencimento: TDate;
begin
  Result := FDataVencimento;
end;

function TJeraDACEFBlocoF.GetIdentificacao: Int64;
begin
  Result := FIdentificacao;
end;

function TJeraDACEFBlocoF.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDACEFBlocoF.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDACEFBlocoF.GetTipoIdentificacao: TDADIIdentificacao;
begin
  Result := FTipoIdentificacao;
end;

function TJeraDACEFBlocoF.GetUsoEmpresa: string;
begin
  Result := FUsoEmpresa;
end;

function TJeraDACEFBlocoF.GetValorDebito: Currency;
begin
  Result := FValorDebito;
end;

function TJeraDACEFBlocoF.GetValorMora: Currency;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

function TJeraDACEFBlocoF.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoF.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDACEFBlocoF.SetCodigoMovimento(
  const Value: TDACodigoMovimento);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDACEFBlocoF.SetCodigoRetorno(const Value: TDACodigoRetorno);
begin
  case Value of
    ccr47DebitoNaoEfetuadoValorDebitoAcimaLimite,
      ccr48DebitoNaoEfetuadoLimiteDiarioDebitoUltrapassado,
      ccr49DebitoNaoEfetuadoCnpjCpfDebitadoInvalido,
      ccr50DebitoNaoEfetuadoCnpjCpfNaoPertenceContaDbitada:
      raise Exception.Create('Valor atribu�do no C�digo de retorno n�o pode ser utilizado para o banco atual!');
  end;
  FCodigoRetorno := Value;
end;

procedure TJeraDACEFBlocoF.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDACEFBlocoF.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDACEFBlocoF.SetDataVencimento(const Value: TDate);
begin
  FDataVencimento := Value;
end;

procedure TJeraDACEFBlocoF.SetIdentificacao(const Value: Int64);
begin
  FIdentificacao := Value;
end;

procedure TJeraDACEFBlocoF.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDACEFBlocoF.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDACEFBlocoF.SetTipoIdentificacao(
  const Value: TDADIIdentificacao);
begin
  FTipoIdentificacao := Value;
end;

procedure TJeraDACEFBlocoF.SetUsoEmpresa(const Value: string);
begin
  FUsoEmpresa := Value;
end;

procedure TJeraDACEFBlocoF.SetValorDebito(const Value: Currency);
begin
  FValorDebito := Value;
end;

procedure TJeraDACEFBlocoF.SetValorMora(const Value: Currency);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Valor de Mora');
end;

{ TJeraDACEFBlocoH }

procedure TJeraDACEFBlocoH.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDACEFBlocoH.Create;
begin
  inherited Create;
  FCodigoRegistro := 'H';
end;

function TJeraDACEFBlocoH.GetAgenciaDebito: string;
begin
  Result := FAgenciaDebito;
end;

function TJeraDACEFBlocoH.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoH.GetBrancos2: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Brancos2');
end;

function TJeraDACEFBlocoH.GetCodigoMovimento: TDACodigoMovimentoBlocoD;
begin
  Result := FCodigoMovimento;
end;

function TJeraDACEFBlocoH.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoH.GetConta: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

function TJeraDACEFBlocoH.GetContaDigito: string;
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

function TJeraDACEFBlocoH.GetIdentificacaoClienteBanco: string;
begin
  Result := FIdentificacaoClienteBanco;
end;

function TJeraDACEFBlocoH.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDACEFBlocoH.GetIdentificacaoClienteEmpresaAnterior: string;
begin
  Result := FIdentificacaoClienteEmpresaAnterior;
end;

function TJeraDACEFBlocoH.GetOcorrencia: string;
begin
  Result := FOcorrencia;
end;

function TJeraDACEFBlocoH.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoH.SetAgenciaDebito(const Value: string);
begin
  FAgenciaDebito := Value;
end;

procedure TJeraDACEFBlocoH.SetCodigoMovimento(
  Value: TDACodigoMovimentoBlocoD);
begin
  FCodigoMovimento := Value;
end;

procedure TJeraDACEFBlocoH.SetConta(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'Conta');
end;

procedure TJeraDACEFBlocoH.SetContaDigito(const Value: string);
begin
  raise Exception.Create('N�o aplica para a vers�o do Layout informado!' + sLineBreak +
                         'D�gito da Conta');
end;

procedure TJeraDACEFBlocoH.SetIdentificacaoClienteBanco(const Value: string);
begin
  FIdentificacaoClienteBanco := Value;
end;

procedure TJeraDACEFBlocoH.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDACEFBlocoH.SetIdentificacaoClienteEmpresaAnterior(
  const Value: string);
begin
  FIdentificacaoClienteEmpresaAnterior := Value;
end;

procedure TJeraDACEFBlocoH.SetOcorrencia(const Value: string);
begin
  FOcorrencia := Value;
end;

{ TJeraDACEFBlocoI }

constructor TJeraDACEFBlocoI.Create;
begin
  inherited Create;
  FCodigoRegistro := 'I';
end;

function TJeraDACEFBlocoI.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoI.GetCidadeConsumidor: string;
begin
  Result := FCidadeConsumidor;
end;

function TJeraDACEFBlocoI.GetCnpjCpf: string;
begin
  Result := FCnpjCpf;
end;

function TJeraDACEFBlocoI.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoI.GetIdentificacaoClienteEmpresa: string;
begin
  Result := FIdentificacaoClienteEmpresa;
end;

function TJeraDACEFBlocoI.GetNomeConsumidor: string;
begin
  Result := FNomeConsumidor;
end;

function TJeraDACEFBlocoI.GetTipoIdentificacao: TDADIIdentificacao;
begin
  Result := FTipoIdentificacao;
end;

function TJeraDACEFBlocoI.GetUFConsumidor: string;
begin
  Result := FUFConsumidor;
end;

function TJeraDACEFBlocoI.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoI.SetCidadeConsumidor(const Value: string);
begin
  FCidadeConsumidor := Value;
end;

procedure TJeraDACEFBlocoI.SetCnpjCpf(const Value: string);
begin
  FCnpjCpf := Value;
end;

procedure TJeraDACEFBlocoI.SetIdentificacaoClienteEmpresa(
  const Value: string);
begin
  FIdentificacaoClienteEmpresa := Value;
end;

procedure TJeraDACEFBlocoI.SetNomeConsumidor(const Value: string);
begin
  FNomeConsumidor := Value;
end;

procedure TJeraDACEFBlocoI.SetTipoIdentificacao(
  const Value: TDADIIdentificacao);
begin
  FTipoIdentificacao := Value;
end;

procedure TJeraDACEFBlocoI.SetUFConsumidor(const Value: string);
begin
  FUFConsumidor := Value;
end;

{ TJeraDACEFBlocoZ }

procedure TJeraDACEFBlocoZ.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

constructor TJeraDACEFBlocoZ.Create;
begin
  inherited Create;
  FCodigoRegistro := 'Z';
end;

function TJeraDACEFBlocoZ.GetBrancos: string;
begin
  FBrancos := '';
  Result := FBrancos;
end;

function TJeraDACEFBlocoZ.GetBrancos2: string;
begin
  FBrancos2 := '000000';
  Result := FBrancos2;
end;

function TJeraDACEFBlocoZ.GetBrancos3: string;
begin
  FBrancos3 := '';
  Result := FBrancos3;
end;

function TJeraDACEFBlocoZ.GetCodigoRegistro: string;
begin
  Result := FCodigoRegistro;
end;

function TJeraDACEFBlocoZ.GetTotalRegistrosArquivo: Integer;
begin
  Result := FTotalRegistrosArquivo;
end;

function TJeraDACEFBlocoZ.GetValorTotalRegistros: Currency;
begin
  Result := FValorTotalRegistros;
end;

function TJeraDACEFBlocoZ.MontarLinha: string;
begin
  Result := TJeraDADIUtils.MontarLinhaFromObj(Self);
end;

procedure TJeraDACEFBlocoZ.SetTotalRegistrosArquivo(const Value: Integer);
begin
  FTotalRegistrosArquivo := Value;
end;

procedure TJeraDACEFBlocoZ.SetValorTotalRegistros(const Value: Currency);
begin
  FValorTotalRegistros := Value;
end;

end.
